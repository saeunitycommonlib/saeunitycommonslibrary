﻿using System.Collections;
using System;
using System.IO;
using System.Linq;

namespace SAEUnityCommonsLibrary.LocalDataStorer
{
    public class LocalDataStorer
    {
        static string directory;
        string fileContents;
        string filePath;

        public LocalDataStorer(string relativeFilePath)
        {
            filePath = Path.Combine(directory, relativeFilePath);

            //make sure the folder the file is in exists
            CreateNecessaryDirectoriesForFilePath();

            GrabFileContents();
        }


        public static void SetDirectory(string directory)
        {
            LocalDataStorer.directory = directory;
        }

        void CreateNecessaryDirectoriesForFilePath()
        {
            string directory = filePath.Substring(0, filePath.LastIndexOf('/'));

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }

        void GrabFileContents()
        {
            FileStream stream = new FileStream(filePath, FileMode.OpenOrCreate);

            StreamReader reader = new StreamReader(stream);

            fileContents = reader.ReadToEnd();

            reader.Close();

            stream.Close();
        }

        int GetIndexOfId(string id)
        {
            string idToLookFor = id + " = ";

            int idIndex = fileContents.IndexOf(idToLookFor);

            if (idIndex != -1)
            {
                idIndex += idToLookFor.Length;
            }

            return idIndex;
        }

        public void SaveFile()
        {
            StreamWriter writer = new StreamWriter(filePath);

            writer.Write(fileContents);

            writer.Close();
        }

        public string GetValue(string id)
        {
            int idIndex = GetIndexOfId(id);

            if (idIndex == -1)
            {
                return null;
            }

            int lengthUntilEndOfLine = GetLengthUntilEndOfLine(idIndex);

            string valueString = fileContents.Substring(idIndex, lengthUntilEndOfLine);

            return valueString;
        }

        public void SetValue(string id, string value)
        {
            int idIndex = GetIndexOfId(id);

            //if the id doesn't exist, add the id and value to the file contents
            if (idIndex == -1)
            {
                fileContents += id + " = " + value + System.Environment.NewLine;

                return;
            }

            int lengthUntilEndOfLine = GetLengthUntilEndOfLine(idIndex);

            //we know the id exists, therefore, replace the value
            fileContents = fileContents.Remove(idIndex, lengthUntilEndOfLine);
            fileContents = fileContents.Insert(idIndex, value);
        }

        int GetLengthUntilEndOfLine(int idIndex)
        {
            //we need to know the index the next end of line character will be at
            int lengthUntilEndOfLine = fileContents.IndexOf(System.Environment.NewLine, idIndex);
            //subtracting the idIndex gives us the difference in location of the end of line and idIndex which will be the number we need
            lengthUntilEndOfLine -= idIndex;

            return lengthUntilEndOfLine;
        }
    }
}