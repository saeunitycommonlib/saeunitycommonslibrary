﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace SAEUnityCommonsLibrary.LocalDataStorer
{
    [CustomEditor(typeof(Example))]
    public class ExampleDrawer : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            Example instance = (Example)target;

            DrawDefaultInspector();

            if (Application.isPlaying)
            {
                if (GUILayout.Button("Save Player Names"))
                {
                    instance.SetPlayers();
                }
                if (GUILayout.Button("Set Values"))
                {
                    instance.SetData();
                }
                if (GUILayout.Button("Get Values"))
                {
                    instance.GetData();
                }
            }

        }
    }
}