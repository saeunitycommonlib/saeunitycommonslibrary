﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace SAEUnityCommonsLibrary.LocalDataStorer
{
    public class Example : MonoBehaviour
    {
        LocalDataStorer listOfPlayers;
        LocalDataStorer[] playerData;
        string directory;

        //values to set
        public string idToSave = "Health";
        public string[] playerValues;
        public string[] playerNames;

        void Start()
        {
            SetMainDirectory();

            listOfPlayers = new LocalDataStorer("Players");

            GetPlayers();
        }

        void SetMainDirectory()
        {
            //if SetDirectory isn't called, then whenever a local data storer is initialised, you must include the rest of the directory
            directory = Path.Combine(Application.dataPath, "LocalDataStorer/ExampleFiles");
            LocalDataStorer.SetDirectory(directory);
        }

        public void SetPlayers()
        {
            for (int i = 0; i < playerNames.Length; i++)
            {
                listOfPlayers.SetValue("Player " + i.ToString(), playerNames[i]);
            }

            listOfPlayers.SaveFile();

            GetPlayers();
        }

        void GetPlayers()
        {
            SetMainDirectory();

            //find the number of players in the file so as to allocate the correct amount of memory
            int numberOfPlayers = 0;

            while (true)
            {
                if (listOfPlayers.GetValue("Player " + numberOfPlayers.ToString()) != null)
                {
                    //there is a player!
                    numberOfPlayers++;
                }
                else
                {
                    //there are no more players!
                    break;
                }
            }

            //allocate the memory
            playerData = new LocalDataStorer[numberOfPlayers];
            playerValues = new string[numberOfPlayers];

            //go into the "PlayerFiles" directory
            directory = Path.Combine(directory, "PlayerFiles");
            LocalDataStorer.SetDirectory(directory);

            //create the playerData values
            for (int i = 0; i < numberOfPlayers; i++)
            {
                string playerFileName = listOfPlayers.GetValue("Player " + i.ToString());

                playerData[i] = new LocalDataStorer(playerFileName);
            }

            GetData();
        }

        public void SetData()
        {
            for (int i = 0; i < playerData.Length; i++)
            {
                playerData[i].SetValue(idToSave, playerValues[i].ToString());
                playerData[i].SaveFile();
            }
        }

        public void GetData()
        {
            for (int i = 0; i < playerData.Length; i++)
            {
                playerValues[i] = playerData[i].GetValue(idToSave);
            }
        }
    }
}